import { createApp, reactive } from 'https://unpkg.com/petite-vue?module'

const data = {
    'item1': {
        id: "ID001",
        name: "ItemName1",
        description: "Item description goes here"
    },
    'item2': {
        id: "ID002",
        name: "ItemName2",
        description: "Item description goes here"
    },
    'item3': {
        id: "ID003",
        name: "ItemName3",
        description: "Item description goes here"
    }
}

createApp({
    data: data,

    activeTab: "Goal",

    showAddModal: false,
    showEditModal: false,
    showConfirmationModal: false,

    activeItem: "",
    activeItemData: {
        id: "",
        name: "",
        description: "",
        format: ""
    },

    modalContext: {},

    updateTab(tabName) {
        this.activeTab = tabName;
    },

    submitAddItem() {
        this.showAddModal = false;
        this.modalContext.title = "Addition";
        this.modalContext.acceptText = "Add";
        this.modalContext.body = "add a " + this.activeTab;
        this.toggleConfirmationModal();
    },

    submitEditItem() {
        this.showEditModal = false;
        this.modalContext.title = "Update";
        this.modalContext.acceptText = "Update";
        this.modalContext.body = "update a " + this.activeTab;
        this.toggleConfirmationModal();
    },

    openEditModal(itemName) {
        this.showEditModal = true;
        this.activeItem = itemName;
    },

    openDeleteModal(itemName) {
        this.modalContext.title = "Deletion";
        this.modalContext.itemName = itemName;
        this.modalContext.acceptText = "Delete";
        this.modalContext.body = "delete " + this.activeTab + ": " + itemName;
        this.toggleConfirmationModal();
    },

    toggleConfirmationModal() {
        this.showConfirmationModal = !this.showConfirmationModal;
    },

    confirmationModalCancel() {
        this.showConfirmationModal = false;
    },

    confirmationModalAccept() {
        this.toggleConfirmationModal();
    },
}).mount()
